import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AppRouterModule } from '../app-router/app-router.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class CoreModule { }
