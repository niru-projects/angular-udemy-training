import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'warning-alert',
  template: `
    <div>Warning alert</div>
  `,
  styles: [`
    div {
      background-color: yellow;
      padding: 20px;
      margin: 10px;
    }
  `]
})
export class WarningAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
