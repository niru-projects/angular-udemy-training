import { Component, OnInit } from '@angular/core';
import { Observable ,  Subject ,  Observer } from 'rxjs';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
    
  constructor(private breakpointObserver: BreakpointObserver, private router: Router) {}

  ngOnInit() {
    

    // const subject = new Subject();

    // subject.subscribe((data : any) => {
    //   console.log("string: ", data);
    // });

    // subject.next(1);
    // subject.next(2);

    // let valor = true;
    // const observable = Observable.create((observer : Observer<any>) => {
    //   if(valor) {
    //     observer.next(3);
    //   } 
    //   valor = !valor;
    //   return observer;
    // });

    // observable.subscribe(subject);
  }
}
