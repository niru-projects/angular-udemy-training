import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styles: [`
    .textColor1 {
      color: black;
    }
    .textColor2 {
      color: white;
    }
  `]
})
export class InfoComponent implements OnInit {

  username : string;
  buttonText : string = 'Display username'; 
  showUsername : boolean = false;
  buttonClicks = [];

  constructor() { }

  ngOnInit() {
  }

  onClickSave() {
    this.username = '';
  }

  onClickDisplay() {
    this.showUsername = !this.showUsername;
    if(this.showUsername) {
      this.buttonText = 'Hide username';
    } else {
      this.buttonText = 'Display username';
    }

    this.buttonClicks.push("Button clicked " + (this.buttonClicks.length + 1) + " times.");
  }

  disableButton() : boolean {
    return !this.username || this.username.length == 0;
  }

  getLogBackgroundColor(index : any) : string {
    if((index+1) > 4) {
      return 'blue';
    }
    return 'transparent';
  }
}
